import os,sys
import json
import time
import calendar
import datetime
from tornado import ioloop,web
import urllib
import redis,ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
from demo import demo

__UPLOADS__ = "static/uploads/"

class PicsHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        picno        = int(self.get_argument('picno'))
        doctor_array = []
        high_array   = []
        low_array    = [] 
        
        if picno is None:
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':0,'msg':'Invalid Pic Number'}))
            return

        if picno >= 81 and picno <= 120:
            main_image = 'utilities.paralleldots.com/wholeimages/'+str(picno)+'.jpg'
            doctor     = 'static/ToothImages/'+str(picno)+'/img'
            high       = 'static/ToothImages/'+str(picno)+'/high'
            low        = 'static/ToothImages/'+str(picno)+'/low'

            doctor_images_list = sorted(os.listdir(doctor))
            high_images_list   = sorted(os.listdir(doctor))
            low_images_list    = sorted(os.listdir(doctor))

            for i in range(0,len(doctor_images_list)):
                doctor_array.append('utilities.paralleldots.com/toothimages/'+str(picno)+'/img/'+str(doctor_images_list[i]))

            for i in range(0,len(high_images_list)):
                high_array.append('utilities.paralleldots.com/toothimages/'+str(picno)+'/high/'+str(high_images_list[i]))

            for i in range(0,len(low_images_list)):
                low_array.append('utilities.paralleldots.com/toothimages/'+str(picno)+'/low/'+str(low_images_list[i]))        


            if picno == 120:
                picno = 81
            else:
                picno = picno+1    
            
            data = {'main_image':main_image,'doctor':doctor_array,'high':high_array,'low':low_array}


            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':1,'next':'http://utilities.paralleldots.com/api/images/prediction?picno='+str(picno),'data':data}))
            return
        else:
            self.set_header("Content-Type", "application/json")
            self.set_status(200)
            self.finish(json.dumps({'Status':0,'msg':'Invalid Pic Number'}))
            return


class PicsUploadHandler(web.RequestHandler):
    def post(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        fileinfo        = self.request.files['file1'][0]
        doctor_id       = self.get_body_argument("doc_id") 
        fname           = fileinfo['filename']                
        extn            = os.path.splitext(fname)[1]
        print extn
        print fname
        fh    = open(__UPLOADS__ + fname, 'w')
        fh.write(fileinfo['body'])
        
        # fname     = fname.split(".")
        # fname     = fname[0]
        print fname 
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        self.finish(json.dumps({'Status':1,'image':"http://192.168.0.161:8001/static/uploads/"+str(fname)}))
        return 


settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : False
}

application = web.Application([
    (r'/api/images/prediction', PicsHandler),
    (r'/api/picture/upload', PicsUploadHandler),
],**settings)

if __name__ == "__main__":
    print "Here we go"
    application.listen(8001)
    ioloop.IOLoop.instance().start()