import glob, os, re, skimage, numpy as np
from skimage.io import imread, imsave
from skimage import color
import skimage.transform
from random import shuffle
image_size = 192
window_size = 30

DEBUG = False

def tryint(s):
	try:
		return int(s)
	except:
		return s

def alphanum_key(s):
	return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
	l.sort(key=alphanum_key)


training_files = sorted(glob.glob('TestSegmentedToothImages/*.jpg'))
sort_nicely(training_files)

sampled_files = []

def sampled_dataset():
	for img_num in xrange(len(training_files)):
		sampled_files.append([training_files[img_num]])
	return sampled_files

def load_images_and_masks():
	sampled_files = sampled_dataset()
	for num, files in enumerate(sampled_files):
		image_arr = imread(files[0])/255.
		if files[0].split('/')[-1].split('.')[0].split('_')[-1]=='0':
			image_arr = np.flipud(image_arr)
		yield np.asarray(image_arr, dtype=np.float32), files[0]


def rescale(x_size = image_size, y_size =image_size):
	for num, (image, image_file) in enumerate(load_images_and_masks()):
		image = skimage.transform.resize(image,(x_size,y_size))
		yield np.asarray((image).reshape(1,1,image_size,image_size), dtype=np.float32), image_file

if __name__ == '__main__':
	for num, (img_patch, image_file) in enumerate(rescale(x_size = image_size, y_size =image_size)):
		print image_file