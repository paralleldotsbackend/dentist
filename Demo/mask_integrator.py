from skimage.io import imread,imsave
from skimage.color import gray2rgb

import glob,os
import numpy as np

ip = sorted(glob.glob('img/*'))
OP = 'pred/'

result_dir = 'ToothImages/'
if not os.path.exists(result_dir):
	os.makedirs(result_dir)
low = 20./255
high = 50./255
print low,high
alt_method = False
epsilon = 0.05

for imagefile in ip:
	imgname = imagefile.split('/')[-1].split('.')[0]
	print imgname
	imgnum  = imgname.split('_')[0]
	jaw = imgname.split('_')[-1]
	
	#Image
	img = imread(imagefile)
	if len(img.shape) != 3:
		img = gray2rgb(img)
	
	#GroundTruth
	#gt  = imread(GT+imgname+".jpg",as_grey=True)
	#if gt.max()>1:
	#	gt = gt/255.
	
	#Output
	op  = imread(OP+imgname+".jpg",as_grey=True)
	if op.max()>1:
		op = op/255.
	
	op_lw = np.copy(img)
	op_lw[op>=low] = [0,255,0]

	op_hi = np.copy(img)
	op_hi[op>=high] = [0,0,255]

	#Integrated Image
	# img[gt>0.9] = [255,0,0]
	# img = img/255.	
	
	#For inverting the upper jaws
	if jaw=='0':
		img = np.flipud(img)
		op_lw = np.flipud(op_lw)
		op_hi = np.flipud(op_hi)

	#Saving
	sl = result_dir+str(imgnum)+"/"
	imsave(sl+"img/"+imgname+".jpg",img)
	imsave(sl+"low/"+imgname+".jpg",op_lw)
	imsave(sl+"high/"+imgname+".jpg",op_hi)
	if alt_method:
		op[op<(op.max()-epsilon)]=0.
		op[op>0] = 1.
		imsave(sl+"output/"+imgname,op)
	
	
	
