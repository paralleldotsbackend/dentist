import theano, lasagne, theano.tensor as T, numpy as np

num_classes = 3

def create_nn():
	# Input Layer
	patch_in = lasagne.layers.InputLayer((1,1,192,192)) #Takes 1x1x192x192

	# Two back to back conv layers and then MaxPool
	layer1 = lasagne.layers.Conv2DLayer(patch_in,16,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x16x192x192
	layer1_norm =  lasagne.layers.batch_norm(layer1) # 1x16x192x192
	layer1_act = lasagne.layers.NonlinearityLayer(layer1_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x16x192x192

	layer2 = lasagne.layers.Conv2DLayer(layer1_act,16,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x16x192x192
	layer2_norm =  lasagne.layers.batch_norm(layer2)# 1x16x192x192
	layer2_act = lasagne.layers.NonlinearityLayer(layer2_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x16x192x192

	layer2_act_MaxPool = lasagne.layers.MaxPool2DLayer(layer2_act, (2,2)) # 1x16x96x96

	# Two back to back conv layers and then MaxPool
	layer3 = lasagne.layers.Conv2DLayer(layer2_act_MaxPool,32,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x32x96x96
	layer3_norm =  lasagne.layers.batch_norm(layer3) # 1x32x96x96
	layer3_act = lasagne.layers.NonlinearityLayer(layer3_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x32x96x96

	layer4 = lasagne.layers.Conv2DLayer(layer3_act,32,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x32x96x96
	layer4_norm =  lasagne.layers.batch_norm(layer4) # 1x32x96x96
	layer4_act = lasagne.layers.NonlinearityLayer(layer4_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x32x96x96

	layer4_act_MaxPool = lasagne.layers.MaxPool2DLayer(layer4_act, (2,2)) #1x32x48x48

	# Two back to back conv layers and then MaxPool
	layer5 = lasagne.layers.Conv2DLayer(layer4_act_MaxPool,64,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x64x48x48
	layer5_norm =  lasagne.layers.batch_norm(layer5) # 1x64x48x48
	layer5_act = lasagne.layers.NonlinearityLayer(layer5_norm, nonlinearity=lasagne.nonlinearities.rectify) #1x64x48x48

	layer6 = lasagne.layers.Conv2DLayer(layer5_act,64,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x64x48x48
	layer6_norm =  lasagne.layers.batch_norm(layer6) # 1x64x48x48
	layer6_act = lasagne.layers.NonlinearityLayer(layer6_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x64x48x48

	layer6_act_MaxPool = lasagne.layers.MaxPool2DLayer(layer6_act, (2,2)) # 1x64x24x24

	# Two back to back conv layers and then MaxPool
	layer7 = lasagne.layers.Conv2DLayer(layer6_act_MaxPool,128,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x128x24x24
	layer7_norm =  lasagne.layers.batch_norm(layer7) # 1x128x24x24
	layer7_act = lasagne.layers.NonlinearityLayer(layer7_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x128x24x24

	layer8 = lasagne.layers.Conv2DLayer(layer7_act,128,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x128x24x24
	layer8_norm =  lasagne.layers.batch_norm(layer8) # 1x128x24x24
	layer8_act = lasagne.layers.NonlinearityLayer(layer8_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x128x24x24

	layer8_act_MaxPool = lasagne.layers.MaxPool2DLayer(layer8_act, (2,2)) # 1x128x12x12


	# Two back to back conv layers
	layer9 = lasagne.layers.Conv2DLayer(layer8_act_MaxPool,256,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x256x12x12
	layer9_norm =  lasagne.layers.batch_norm(layer9) # 1x256x12x12
	layer9_act = lasagne.layers.NonlinearityLayer(layer9_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x256x12x12

	layer10 = lasagne.layers.Conv2DLayer(layer9_act,256,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x256x12x12
	layer10_norm =  lasagne.layers.batch_norm(layer10) # 1x256x12x12
	layer10_act = lasagne.layers.NonlinearityLayer(layer10_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x256x12x12

	# DeConvolution
	layer11 = lasagne.layers.TransposedConv2DLayer(layer10_act, 128, (2,2), stride=(2, 2), nonlinearity=lasagne.nonlinearities.rectify) # 1x128x24x24

	# Concat Layer 11 and Layer 8 activated
	layer_11_8_act_Concat = lasagne.layers.ConcatLayer([layer11,layer8_act],axis=1,cropping=[None,None,None,None]) # 1x256x24x24

	# Two back to back conv layers
	layer12 = lasagne.layers.Conv2DLayer(layer_11_8_act_Concat,128,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x128x24x24
	layer12_norm =  lasagne.layers.batch_norm(layer12) # 1x128x24x24
	layer12_act = lasagne.layers.NonlinearityLayer(layer12_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x128x24x24

	layer13 = lasagne.layers.Conv2DLayer(layer12_act,128,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x128x24x24
	layer13_norm =  lasagne.layers.batch_norm(layer13) # 1x128x24x24
	layer13_act = lasagne.layers.NonlinearityLayer(layer13_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x128x24x24

	# DeConvolution
	layer14 = lasagne.layers.TransposedConv2DLayer(layer13_act, 64, (2,2), stride=(2, 2), nonlinearity=lasagne.nonlinearities.rectify) # 1x64x48x48

	# Concat Layer 14 and Layer 6 activated
	layer_14_6_act_Concat = lasagne.layers.ConcatLayer([layer14,layer6_act],axis=1,cropping=[None,None,None,None]) # 1x128x48x48

	# Two back to back conv layers
	layer15 = lasagne.layers.Conv2DLayer(layer_14_6_act_Concat,64,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x64x48x48
	layer15_norm =  lasagne.layers.batch_norm(layer15) # 1x64x48x48
	layer15_act = lasagne.layers.NonlinearityLayer(layer15_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x64x48x48

	layer16 = lasagne.layers.Conv2DLayer(layer15_act,64,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x64x48x48
	layer16_norm =  lasagne.layers.batch_norm(layer16) # 1x64x48x48
	layer16_act = lasagne.layers.NonlinearityLayer(layer16_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x64x48x48

	# DeConvolution
	layer17 = lasagne.layers.TransposedConv2DLayer(layer16_act, 32, (2,2), stride=(2, 2), nonlinearity=lasagne.nonlinearities.rectify) # 1x32x96x96

	# Concat Layer 17 and Layer 4 activated
	layer_17_4_act_Concat = lasagne.layers.ConcatLayer([layer17,layer4_act],axis=1,cropping=[None,None,None,None]) # 1x64x96x96

	# Two back to back conv layers
	layer18 = lasagne.layers.Conv2DLayer(layer_17_4_act_Concat,32,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x32x96x96
	layer18_norm =  lasagne.layers.batch_norm(layer18) # 1x32x96x96
	layer18_act = lasagne.layers.NonlinearityLayer(layer18_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x32x96x96

	layer19 = lasagne.layers.Conv2DLayer(layer18_act,32,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x32x96x96
	layer19_norm =  lasagne.layers.batch_norm(layer19) # 1x32x96x96
	layer19_act = lasagne.layers.NonlinearityLayer(layer19_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x32x96x96

	# DeConvolution
	layer20 = lasagne.layers.TransposedConv2DLayer(layer19_act, 16, (2,2), stride=(2, 2), nonlinearity=lasagne.nonlinearities.rectify) # 1x16x192x192

	# Concat Layer 20 and Layer 2 activated
	layer_20_2_act_Concat = lasagne.layers.ConcatLayer([layer20,layer2_act],axis=1,cropping=[None,None,None,None]) # 1x32x192x192

	# Two back to back conv layers
	layer21 = lasagne.layers.Conv2DLayer(layer_20_2_act_Concat,16,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x16x192x192
	layer21_norm =  lasagne.layers.batch_norm(layer21) # 1x16x192x192
	layer21_act = lasagne.layers.NonlinearityLayer(layer21_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x16x192x192

	layer22 = lasagne.layers.Conv2DLayer(layer21_act,16,(3,3), stride=1, pad="same", W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x16x192x192
	layer22_norm =  lasagne.layers.batch_norm(layer22) # 1x16x192x192
	layer22_act = lasagne.layers.NonlinearityLayer(layer22_norm, nonlinearity=lasagne.nonlinearities.rectify) # 1x16x192x192

	# 1x1 Convolution with Batch norm and Softmax
	layer23_act_1D_Conv = lasagne.layers.Conv2DLayer(layer22_act,num_classes,(1,1), stride=1,  W = lasagne.init.HeNormal(gain=1.0, c01b=False)) # 1x3x192x192
	layer23_act_1D_Conv_norm =  lasagne.layers.batch_norm(layer23_act_1D_Conv) # 1x3x192x192
	layer23_ds = lasagne.layers.DimshuffleLayer(layer23_act_1D_Conv_norm,(0,2,3,1)) # 1x192x192x3
	layer23_reshape = lasagne.layers.ReshapeLayer(layer23_ds, (36864,num_classes)) # 36864x3
	layer23_reshape_act = lasagne.layers.NonlinearityLayer(layer23_reshape, nonlinearity=lasagne.nonlinearities.softmax) # 36864x3
	layer23_reshape_2 = lasagne.layers.ReshapeLayer(layer23_reshape_act, (1,192,192,num_classes))  # 1x192x192x3
	layer23_ds_2 = lasagne.layers.DimshuffleLayer(layer23_reshape_2,(0,3,1,2)) # 1x3x192x192
	net_output0 = lasagne.layers.get_output(layer23_ds_2) # 1x3x192x192

	test = theano.function(inputs=[patch_in.input_var], outputs=[net_output0])

	return None, test, layer23_ds_2



if __name__ == '__main__':
	print "compiling"
	create_nn()
	print "compiled"