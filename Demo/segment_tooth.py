from isolater_code import segment_image
import numpy as np
import glob,os,re,csv
import skimage
from skimage.io import imread,imsave
from skimage.transform import rotate

def tryint(s):
	try:
		return int(s)
	except:
		return s

def alphanum_key(s):
	return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
	l.sort(key=alphanum_key)

def savesegment(img_num,oimg):
	eimg,line_theta_y,teethboundaryupper,teethboundarylower = segment_image(oimg)
	roimage = rotate(oimg,line_theta_y[0])

	roimage[line_theta_y[1],:] = 1. #Horizontal Line
	for valley in teethboundaryupper:
		roimage[:line_theta_y[1],valley] = 1.   #Upper Vertical Line

	for valley in teethboundarylower:
		roimage[line_theta_y[1]:,valley] = 1.   #Lower Vertical Line

	if not os.path.exists('test_seg_whole_img'):
		os.makedirs('test_seg_whole_img')
	filename = "test_seg_whole_img/" + str(img_num) + ".jpg"
	imsave(filename,roimage)

def segment_tooth(imagepath):

	pad = 20
	threshold = 80

	# CREATE FOLDERS FOR OUTPUT
	imglocation = 'TestSegmentedToothImages/'
	if not os.path.exists(imglocation):
		os.makedirs(imglocation)

	oimg = imread(imagepath, as_grey=True)
	img_num = imagepath.split('/')[-1].split('.')[0]

	savesegment(img_num,oimg)
	eimg,line_theta_y,teethboundaryupper,teethboundarylower = segment_image(oimg)
	
	roimage = rotate(oimg,line_theta_y[0])

	teethboundarylower = sorted(list(set(teethboundarylower)))
	teethboundaryupper = sorted(list(set(teethboundaryupper)))
	print teethboundaryupper,teethboundarylower

	print len(teethboundaryupper),len(teethboundarylower)
	n = 0
	## Segment teeth out from upper jaw
	for i in xrange(len(teethboundaryupper)):

		if i==0 and (teethboundaryupper[0] > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_0.jpg"
			imsave(filename,roimage[:line_theta_y[1],:teethboundaryupper[0]+pad])
			n+=1

		if i>0 and ((teethboundaryupper[i] - teethboundaryupper[i-1]) > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_0.jpg"
			if (teethboundaryupper[i-1]-pad >= 0) and (teethboundaryupper[i]+pad <= roimage.shape[1]):
				imsave(filename,roimage[:line_theta_y[1],teethboundaryupper[i-1]-pad:teethboundaryupper[i]+pad])					
				n+=1

		if i==(len(teethboundaryupper)-1) and ((roimage.shape[1] - teethboundaryupper[i]) > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_0.jpg"
			imsave(filename,roimage[:line_theta_y[1],teethboundaryupper[i]-pad:])
			n+=1
		

	for i in xrange(len(teethboundarylower)):

		if i==0 and (teethboundarylower[0] > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_1.jpg"
			imsave(filename,roimage[line_theta_y[1]:,:teethboundarylower[0]+pad])
			n+=1
		if i>0 and ((teethboundarylower[i] - teethboundarylower[i-1]) > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_1.jpg"
			if (teethboundarylower[i-1]-pad >= 0) and (teethboundarylower[i]+pad <= roimage.shape[1]):
				imsave(filename,roimage[line_theta_y[1]:,teethboundarylower[i-1]-pad:teethboundarylower[i]+pad])
				n+=1

		if i==(len(teethboundarylower)-1) and ((roimage.shape[1] - teethboundarylower[i]) > threshold):
			filename = imglocation + str(img_num) + "_" + str(n) + "_1.jpg"
			imsave(filename,roimage[line_theta_y[1]:,teethboundarylower[i]-pad:])
			n+=1

if __name__=="__main__":

	segment_tooth()

	
