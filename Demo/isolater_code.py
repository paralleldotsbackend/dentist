import skimage.io
import skimage.morphology
from skimage.transform import rotate
import numpy as np 

def enhance_image(imgarray):
	imgwidth = imgarray.shape[1]
	selem = skimage.morphology.disk(imgwidth/20)
	bottom_hat = skimage.morphology.closing(imgarray,selem) - imgarray
	enhanced = imgarray -  bottom_hat
	return enhanced

def get_horizontal_partition(imgarray,anglerange=(-45,45,1),skip = 0.2):
	rotated = []
	vallies = []
	valleypos = []
	horis = []
	angles = []
	lowestang = -1
	lowval = np.inf
	for numangle,angle in enumerate(xrange(*anglerange)):
		angles.append(angle)
		roti = rotate(imgarray,angle,mode= "constant",cval = 1.)
		rotated.append(roti)
		horitrans = roti.sum(axis=1)
		#horitrans = (roti != 0).sum(1)
		lowlim = skip * len(horitrans)
		uplim = (1-skip) * len(horitrans)
		horitrans[:lowlim] = 1000000
		horitrans[uplim:] = 1000000
		#skimage.io.imsave("tilted_"+str(numangle)+"arr.bmp",roti)
		horis.append(horitrans)
		valley = horitrans.min()
		valleyp = horitrans.argmin()
		valleypos.append(valleyp)
		vallies.append(valley)
		if valley < lowval:
			lowval = valley
			lowestang = numangle
	assert len(rotated) == len(vallies) == len(horis) == len(angles) == len(valleypos)
	assert lowestang != -1
	# print valleypos[lowestang] , "is Y coordinate", vallies[lowestang], "is value"
	finangle = angles[lowestang]
	y = valleypos[lowestang]
	return finangle,y

def get_vertical_partition(imgarray,line_theta_y,N =5,minsize = 100, delta = 200):
	# MinSize is minium possible size of window, all windows smaller than minsize are discarded, N is the moving average window
	roimage = rotate(imgarray,line_theta_y[0])
	intercept = line_theta_y[1]
	# print roimage.shape,intercept
	V1 = roimage[:intercept][:].sum(axis=0)
	V2 = roimage[intercept:][:].sum(axis=0)
	V1av = np.convolve(V1, np.ones((N,))/N, mode='valid')
	V2av = np.convolve(V2, np.ones((N,))/N, mode='valid')
	V1grad = np.gradient(V1av)
	V2grad = np.gradient(V2av)
	WindowsUpper = []
	WindowsLower = []
	# Upper Teeth Window
	for i in xrange(V1grad.shape[0]):
		if i==0:
			prevgrad = V1grad[i]
			continue
		thisgrad = V1grad[i]
		if prevgrad > 0 and thisgrad < 0:
			dontdoanythingtill = i + delta
			if dontdoanythingtill >= V1grad.shape[0]:
				continue
			if V1grad[dontdoanythingtill]>=0:
				for i2 in xrange(dontdoanythingtill,V1grad.shape[0]):
					if V1grad[i2]<0:
						WindowsUpper.append([i,i2])
						break
			else:
				for i2 in xrange(i,dontdoanythingtill):
					if V1grad[i2]>=0:
						WindowsUpper.append([i,i2])
						break
		prevgrad = thisgrad
	ValliesUpper = []
	for window in WindowsUpper:
		assert window[1]> window[0]
		if window[1]-window[0]<minsize:
			continue
		valley = V1av[window[0]:window[1]].argmin() + window[0]
		ValliesUpper.append(valley)

	#lower Teeth Window
	for i in xrange(V2grad.shape[0]):
		if i==0:
			prevgrad = V2grad[i]
			continue
		thisgrad = V2grad[i]
		if prevgrad > 0 and thisgrad < 0:
			dontdoanythingtill = i + delta
			if dontdoanythingtill >= V2grad.shape[0]:
				continue
			if V2grad[dontdoanythingtill]>=0:
				for i2 in xrange(dontdoanythingtill,V2grad.shape[0]):
					if V2grad[i2]<0:
						WindowsLower.append([i,i2])
						break
			else:
				for i2 in xrange(i,dontdoanythingtill):
					if V2grad[i2]>=0:
						WindowsLower.append([i,i2])
						break
		prevgrad = thisgrad
	ValliesLower = []
	for window in WindowsLower:
		assert window[1]> window[0]
		if window[1]-window[0]<minsize:
			continue
		valley = V2av[window[0]:window[1]].argmin()+ window[0]
		ValliesLower.append(valley)



	return ValliesUpper,ValliesLower

def segment_image(imgarray):
	eimg = enhance_image(imgarray)
	line_theta_y = get_horizontal_partition(eimg)
	teethboundaryupper,teethboundarylower = get_vertical_partition(eimg,line_theta_y)
	return eimg,line_theta_y,teethboundaryupper,teethboundarylower

if __name__=="__main__":

	### Example of how to use
		imgarray = skimage.io.imread(image,as_grey = True)
		oimg,line_theta_y,teethboundaryupper,teethboundarylower = segment_image(imgarray)

	### Saving the image with demarkations
		roimage = rotate(oimg,line_theta_y[0])
		roimage[line_theta_y[1],:] = 1. #Horizontal Line
		for valley in teethboundaryupper:
			roimage[:line_theta_y[1],valley] = 1.   #Upper Vertical Line

		for valley in teethboundarylower:
			roimage[line_theta_y[1]:,valley] = 1.   #Lower Vertical Line

		skimage.io.imsave(filename,roimage)

