import etl_demo, unet_demo
import cPickle, numpy as np, lasagne, os	
from skimage.io import imsave
train, test, layer23_ds_2 = unet_demo.create_nn()

image_size = 192
window_size = 30
epochs = 1

DEBUG = False

low = 20./255
high = 50./255

def makedir(folder):
	if not os.path.exists(folder):
		os.makedirs(folder)

order = ['cavity','boneloss','background'] # Check conventions.csv for verification

def testing():

	global epochs
	out = open('weights/wholeimg_weights.pkl')
	data = cPickle.load(out)
	out.close()

	lasagne.layers.set_all_param_values(layer23_ds_2,data)
	for epoch in xrange(epochs):
		for num, (img_patch, image_file) in enumerate(etl_demo.rescale(x_size = image_size, y_size =image_size)):
			net_output0 = test(img_patch)
			net_output0 = np.asarray(net_output0)
			
			'''
			for i in xrange(net_output0.shape[1]):
				print order[i] + "==> Max: " + str((np.asarray(net_output0[0,i,:,:])).max()) + " Min: " + str((np.asarray(net_output0[0,i,:,:])).min()) + " Mean: " + str((np.asarray(net_output0[0,i,:,:])).mean())

			print "Image " + image_file + " Done. Epochs done: " +str(epoch)+ ". Image num: " + str(num) + " Done." 
			print " "
			'''
			img = img_patch[0,0,:,:]
			op_hi = np.copy(net_output0[0,0,0,:,:])
			op_hi[op_hi >= high] = 1.
			op_hi[op_hi < high] = 0.

			filename = str(image_file.split('/')[-1])
			imagename = str(filename.split('.')[0])
			jaw = str(imagename.split('_')[-1])

			if jaw == '0':
				img = np.flipud(img)
				op_hi = np.flipud(op_hi)
				

			ipfolder = "static/ToothImages/" + str(imagename.split('_')[0])+"/img/"
			opfolder = "static/ToothImages/" + str(imagename.split('_')[0])+"/high/"
			makedir(ipfolder)
			makedir(opfolder)

			for i in xrange(net_output0.shape[2]-1): #not printing background basically
				imsave(opfolder+imagename +'.jpg',op_hi) #'_'+ order[i]
				break
			imsave(ipfolder+filename,img)	

	return "static/ToothImages/" + str(imagename.split('_')[0])
				
if __name__ == '__main__':
	
	testing()